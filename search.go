package main

import (
	"fmt"
)

func main() {
	/*
		list := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}

		fmt.Printf("list: %v\n", list)
		target := 4

		fmt.Println("target is", target)
		fmt.Println(binsearch(list, target))
	*/

	list := []int{32, 21, 233, 49, 67, 46, 76, 28, 93, 123} // len=10
	hsls := mkhsls(list)                                    // len=20
	fmt.Println(hsls)
	fmt.Println(srchhs(hsls, 21))

}

// インデックスを返す
// 見つからなければ-1
func linsearch(list []int, target int) int {
	for i := 0; i < len(list); i++ {
		if list[i] == target {
			return i
		}
	}
	return -1
}

func binsearch(list []int, target int) int {
	var middle int

	head := 0
	tail := len(list) - 1

	for head < tail {
		// スライスのインデックスは0始まりなので偶奇が逆になる
		if (head+tail)%2 == 0 {
			middle = (head + tail) / 2
		} else {
			middle = (head+tail)/2 + 1
		}

		// found
		if list[middle] == target {
			return middle
		}

		if list[middle] < target {
			head = middle + 1
		} else {
			tail = middle - 1
		}
	}
	return -1
}

// 2017-08-18
func mkhsls(list []int) (hsls []int) {
	// 受け取ったスライスに0が含まれていたらうまくいかなくなる

	orilen := len(list)          // original length
	hsls = make([]int, 2*orilen) // hashlist
	hslen := len(hsls)           // hashlist length
	var hsi int                  // hashindex

	// ハッシュ関数はmod(len(hsls))で
	for _, v := range list {
		// vはlist[i]のほうがわかりやすい？
		hsi = v % hslen
		// ここで==としてしまって無限ループに陥ってうまくいかなかった
		for hsls[hsi] != 0 {
			hsi = (hsi + 1) % hslen
		}
		hsls[hsi] = v
	}
	return hsls
}

// 2017-08-18
func srchhs(hsls []int, target int) (indenx int) {
	hslen := len(hsls)   // hash list length
	hs := target % hslen // hash

	// hashlistは必ず0を持っているという前提
	// len(hashlist) > len(originalList)なので、0で初期化されたままのところがあるはず
	for hsls[hs] != 0 {
		if hsls[hs] == target {
			return hs
		}
		hs = (hs + 1) % hslen
	}
	return -1

	/* 自分で考えた終了条件は以下の通り

	iniths := hs	// いちばん最初のハッシュを持っておいて、hsのインクリメントが無限ループに入らないようにする
	// いきなり for iniths != hs {} としてしまうとループ処理に入れない
	// do-while的にやりたい
	if hsls[hs] == target {
		return hs
	}
	hs = (hs + 1) % hslen

	for hs != iniths {
		if hsls[hs] == target {
			return hs
		}
		hs = (hs + 1) % hslen
	}
	return -1
	*/

	/* クロージャをつかったらこんな風にも書けそう

	iniths := hs
	f := func() {
		if hsls[hs] == target {
			return hs
		}
		hs = (hs + 1) % hslen
	}
	f()
	for iniths != hs {
		f()
	}
	return -1
	*/

	// do-whileしたいなら以下を参照
	// http://qiita.com/mizukmb/items/c930de511fb3bdcddce9

}
